# Distribuciones

En el ámbito del control de versiones, diversas plataformas y servicios han integrado Git para facilitar su implementación y mejorar la colaboración. Algunas de las distribuciones y plataformas más destacadas son:

- **GitHub**: Una plataforma basada en la nube que utiliza Git y ofrece servicios adicionales como seguimiento de problemas, colaboración en equipo y despliegue continuo. GitHub ha ganado popularidad como un espacio central para proyectos de código abierto y privados.

- **GitLab**: Similar a GitHub, GitLab proporciona servicios de control de versiones basados en Git, junto con herramientas adicionales para la gestión completa del ciclo de vida del software. GitLab se destaca por ofrecer tanto una versión en la nube como una versión de instalación local, brindando flexibilidad a los equipos.

- **Bitbucket**: Ofrece servicios de control de versiones compatibles con Git y Mercurial. Proporciona integración con otras herramientas de Atlassian, como Jira, y es conocido por su capacidad para gestionar tanto repositorios de código fuente como proyectos relacionados.

Estas plataformas simplifican la gestión de versiones, favorecen la colaboración en equipos distribuidos y proporcionan herramientas para el seguimiento de problemas e integración continua. La elección entre ellas depende de las necesidades y preferencias específicas del equipo en cuanto a características y flujo de trabajo.

![image](/img/distribuciones.png)
# control-versiones

![image](/img/portada.jpg)

## Indice
1. [Introduccion](/contenido/introduccion.md)
2. [Caracteristicas](/contenido/caracteristicas.md)
3. [Distribuciones](/contenido/distribuciones.md)

## Referencias
[keepcoding](https://keepcoding.io/)

[Git](https://git-scm.com/book/es/v2/Inicio---Sobre-el-Control-de-Versiones-Acerca-del-Control-de-Versiones)

## Autores

Jefe de proyecto:
- [Daniel Tenorio](https://gitlab.com/DanielTenorioF)

Colaborador: 
- [Freddy Lopez](https://gitlab.com/freddy13513)

# Licencia

![image](/img/licenciaPortada.PNG)